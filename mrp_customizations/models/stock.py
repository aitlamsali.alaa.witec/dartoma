from odoo import api, fields, models, _


class ProductionLot(models.Model):
    _inherit = "stock.production.lot"

    available_quantity = fields.Float(compute="_compute_available_quantity", store=True)

    @api.depends("quant_ids", "quant_ids.available_quantity")
    def _compute_available_quantity(self):
        for lot in self:
            # Odoo computes lot's available quantity as a view filter using a search method (_search_on_hand).
            # For reliability, we trigger the same method.
            quant_mdl = self.env["stock.quant"]
            on_hand_search = quant_mdl.search(quant_mdl._search_on_hand("=", True))
            on_hand_lot_quants = on_hand_search.filtered_domain([("lot_id", "=", lot.id)])
            lot.available_quantity = sum(on_hand_lot_quants.mapped("available_quantity"))
    #
    # @api.depends("name", "available_quantity", "product_qty")
    # def _compute_display_name(self):
    #     for lot in self:
    #         lot.display_name = _("%(name)s  (%(available)s / %(on_hand)s)") % {
    #             "name": lot.name,
    #             "available": lot.available_quantity,
    #             "on_hand": lot.product_qty,
    #         }

    def name_get(self):
        result = []
        for record in self:
            rec_name = "%s > %s / %s" % (record.name, round(record.product_qty,2), round(record.available_quantity,2 ))
            result.append((record.id, rec_name))
        return result

"""
class StockMoveLine(models.Model):
    _inherit = "stock.move.line"

    lot_id = fields.Many2one(domain=[("available_quantity", ">", 0)])
"""

"""    def action_show_details(self):
        self.ensure_one()
        action = super().action_show_details()
        action["domain"] = [("lot_ids.available_quantity", ">", 0)]
        return action
"""



















