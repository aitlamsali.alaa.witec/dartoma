{
    'name': "MRP Customizations",
    'summary': "General MRP Customizations.",
    'description': "This module holds general MRP Customizations.",
    'author': "Mustafa Salim",
    'website': "mssenol95@gmail.com",
    'license': "Other proprietary",
    'category': "Manufacturing/Manufacturing",
    'version': "14.0.1.0",
    'depends': ["stock", "mrp"],
    'data': [
        "views/views.xml",
    ],
    'application': False,
    'installable': True,
}
