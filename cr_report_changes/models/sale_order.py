from odoo import api,models,fields,_ 
from odoo.osv import expression

class AccountMove(models.Model):
    _inherit = 'account.move.line'

    total_items = fields.Float(string="Total Unités",compute="_compute_total_items")

    @api.depends('quantity','product_id')
    def _compute_total_items(self):
        
        self.total_items = 0.0
        for rec in self:
            bom_id = self.env['mrp.bom'].search([('product_id','=',rec.product_id.id),('type','=','phantom')],limit=1)
            total_quantity = 0
            if bom_id:
                total_quantity = max(bom_id.bom_line_ids.mapped('product_qty'))
            
            rec.total_items = total_quantity * rec.quantity

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    total_items = fields.Float(string="Total Unités",compute="_compute_total_items")

    @api.depends('product_uom_qty','product_id')
    def _compute_total_items(self):
        
        self.total_items = 0.0
        for rec in self:
            bom_id = self.env['mrp.bom'].search([('product_id','=',rec.product_id.id),('type','=','phantom')],limit=1)
            total_quantity = 0
            if bom_id:
                total_quantity = max(bom_id.bom_line_ids.mapped('product_qty'))
            rec.total_items = total_quantity * rec.product_uom_qty
        

class StockMove(models.Model):
    _inherit = 'stock.move'

    total_items = fields.Float(string="Total Unités",compute="_compute_total_items")

    @api.depends('product_uom_qty','product_id')
    def _compute_total_items(self):
        
        self.total_items = 0.0
        for rec in self:
            bom_id = self.env['mrp.bom'].search([('product_id','=',rec.product_id.id),('type','=','phantom')],limit=1)
            total_quantity = 0
            if bom_id:
                total_quantity = max(bom_id.bom_line_ids.mapped('product_qty'))
            rec.total_items = total_quantity * rec.product_uom_qty

class AccountMove(models.Model):
    _inherit = 'account.move'

    def get_delivery_details(self,data):
        sale_order = self.env['sale.order'].search([('name','=',data.invoice_origin)],limit=1)
        delivery_number = False
        delivery_date = False
        if sale_order:
            stock_picking = self.env['stock.picking'].search([('group_id.name','=',sale_order.name)])
            delivery_number = stock_picking.mapped('name')
            delivery_number = ",".join(delivery_number)
            date = []
            for rec in stock_picking:
                if rec.date_done:
                    date.append(str(rec.date_done.date()))
            delivery_date = ",".join(date)
        
        return {
            'delivery_number':delivery_number,
            'delivery_date':delivery_date,
            'customer_reference':sale_order.client_order_ref if sale_order else False,
            'order_date':sale_order.date_order if sale_order else False,
            'contract_number':data.partner_id.contract_number
        }


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def get_so_delivery_details(self, data):
        sale_order = self.env['sale.order'].search([('name', '=', data.invoice_origin)], limit=1)
        delivery_number = False
        delivery_date = False
        if self.picking_ids:
            stock_picking = self.picking_ids#.search([('group_id.name', '=', sale_order.name)])
            delivery_number = stock_picking.mapped('name')
            delivery_number = ",".join(delivery_number)
            date = []
            for rec in stock_picking:
                if rec.date_done:
                    date.append(str(rec.date_done.date()))
            delivery_date = ",".join(date)

        return {
            'delivery_number': delivery_number,
            'delivery_date': delivery_date,
            'customer_reference': sale_order.client_order_ref if sale_order else False,
            'order_date': sale_order.date_order if sale_order else False,
            'contract_number': data.partner_id.contract_number
        }




class ResPartner(models.Model):
    _inherit = 'res.partner'

    contract_number = fields.Char(string="Contrat N°")

class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    @api.model
    def _name_search(self, name='', args=None, operator='ilike', limit=100, name_get_uid=None):
        print("\n\n\ncontext=====================",self._context)
        args = args or []
        domain = []
        if self._context.get('active_lot_id'):
            result_ids = self._search(expression.AND([domain, args]), order='create_date asc', limit=limit, access_rights_uid=name_get_uid)
            print("result_ids=========",result_ids)
            return self._search(expression.AND([domain, args]), order='create_date asc', limit=limit, access_rights_uid=name_get_uid)
        return super(StockProductionLot, self)._name_search(name=name, args=args, operator=operator, limit=limit, name_get_uid=name_get_uid)

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    def get_picking_value(self,data):
        sale_order = self.env['sale.order'].search([('name','=',data.group_id.name)],limit=1)
        return {

            'delivery_date':data.date_done if data.date_done else False,
            'sale_order':sale_order.name,
            'date_command':data.scheduled_date if data.scheduled_date else False,
            'misc_ref':data.partner_id.ref,
            'contract_number':data.partner_id.contract_number
        }