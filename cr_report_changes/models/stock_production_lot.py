from odoo import api,models,fields,_ 

class StockProductionLot(models.Model):
    _inherit = "stock.production.lot"

    new_pro_qty = fields.Float(string="New Qty", compute="_compute_total_qty", store=True)

    @api.depends('product_qty')
    def _compute_total_qty(self):
        
        self.new_pro_qty = 0.0
        for rec in self:
            rec.new_pro_qty = rec.product_qty