��    +      t  ;   �      �     �  v   �     9     @  	   H  	   R     \     l     �     �     �     �     �     �     �  %   �          !  1   .     `     t     �     �     �     �     �     �  E   �  
   6  7   A  8   y  	   �     �     �     �     �                    4     C     R  1  c     �  v   �     	  	   #	     -	  
   5	     @	  1   V	     �	  
   �	     �	     �	     �	     �	     �	  /   �	     .
     =
  C   J
     �
     �
     �
     �
     �
     �
       /     S   M     �  9   �  F   �     .     4     B     [     x     �     �     �     �  
   �     �            '                   
         "   +                          %   !         *            #                                           (            )                     &       $      	               (update) <span class="fa fa-lg fa-building-o" title="Values set here are company-specific." groups="base.group_multi_company"/> Amount Approve Cancelled Companies Config Settings Confirm sale orders in one step Contact Discount (%) Discount Amount Discount Rate Discount Rate : Discount Type Discount Type : Discount limit requires approval in % Discount type Display Name Get 2 levels of approvals to confirm a sale order Invoices Statistics Journal Entry Journal Item Last Modified on Levels of Approvals Levels of Approvals * Locked Managers must approve discount Minimum discount percentage for which a double validation is required Percentage Percentage of Discount that requires double validation' Provide a double validation mechanism for sales discount Quotation Quotation Sent Sale Discount Approval Sales Analysis Report Sales Order Sales Order Line Status Terms and conditions... Total Discount Untaxed Amount Waiting Approval Project-Id-Version: Odoo Server 14.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-03-31 20:47+0000
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.4.2
 (mise à jour) <span class="fa fa-lg fa-building-o" title="Values set here are company-specific." groups="base.group_multi_company"/> Montant Approuver Annulé Sociétés Paramètres de config Confirmer les ordres de vente en une seule étape Contact Remise (%) Montant de la réduction Taux de réduction Taux d'actualisation : Type de remise Type de remise : La limite d'escompte doit être approuvée en % Type de remise Nom affiché Obtenir deux niveaux d'approbation pour confirmer un ordre de vente Statistiques des factures Pièce comptable Écriture comptable Dernière modification le Niveaux d'approbation Niveaux d'approbation * Bloqué Les gestionnaires doivent approuver les remises Pourcentage minimum de réduction pour lequel une double validation est nécessaire Pourcentage Pourcentage de remise nécessitant une double validation" Fournir un mécanisme de double validation pour les remises sur ventes Devis Devis envoyé Vente Remise Approbation Rapport d'analyse des ventes Bon de commande Ligne de bons de commande Statut Conditions d'utilisation... Remise totale Montant HT Approbation en attente 