# -*- coding: utf-8 -*-
{
    'name': "Add ICE on invoice",

    'summary': u""" ICE  """,
    

    'description': u""" ICE """,

    'author': "WiTeC Technologies",
    'website': "www.witec.ma",
    'category': 'Sales',
    'version': '0.5',
    'depends': [
                'base',
                'contacts',
                'account',
                'sale',
                'sale_management',
                ],
    'data': [
             'views/account_invoice_views.xml',
             'views/res_partner_views.xml',
             'views/sale.xml',
             'report/invoice_report_templates.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': True,
}
