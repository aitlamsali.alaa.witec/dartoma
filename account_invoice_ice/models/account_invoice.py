# -*- coding: utf-8 -*-
from odoo import api, fields, models

class AccountMove(models.Model):
    _inherit = "account.move"

    ice = fields.Char(related="partner_id.ice", string="(ICE) Number", track_visibility='always')
    national_card_number = fields.Char(related="partner_id.national_card_number", string="CIN/IF", track_visibility='always')
    #is_public_customer = fields.Boolean(related='partner_id.is_public_customer', readonly=True)
    
    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if hasattr(super(AccountMove, self), '_onchange_partner_id'):
            res = super(AccountMove, self)._onchange_partner_id()
        self.ice = self.partner_id.ice
