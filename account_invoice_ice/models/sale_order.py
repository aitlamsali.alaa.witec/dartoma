# -*- coding: utf-8 -*-
from odoo import api, fields, models
from datetime import datetime, timedelta


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def _prepare_confirmation_values(self):
        rec = super()._prepare_confirmation_values()
        rec.update({
            'state': 'sale',
            'date_order': self.date_order if self.date_order else fields.Datetime.now()
        })

        return rec
