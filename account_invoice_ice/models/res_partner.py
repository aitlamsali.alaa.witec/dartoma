# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.exceptions import Warning

class ResPartner(models.Model):
    _inherit = "res.partner"

    ice = fields.Char(string=u"ICE", track_visibility='always')
    national_card_number = fields.Char(string=u"ID", track_visibility='always')
    #is_public_customer = fields.Boolean(string="Etablissement Public")
    #M#@api.one
    @api.constrains('ice')
    def _no_chars(self):
        for rec in self :
            if rec.ice and rec.company_type=='company':
                if len(rec.ice) != 15:
                    raise Warning(_(u"ICE lentgh should be 15, Please check !"))

    @api.constrains('ice')
    def _check_unicity_ice(self):
        for rec in self  :
            if rec.ice and rec.company_type=='company' and rec.search_count([('ice', '=', rec.ice)]) > 1:
                raise ValidationError(_("ICE Number should be unique"))
