# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api
from odoo.addons.bose_custom_package.models.egg import get_selected_boxes


import logging

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = "stock.picking"

    def action_add_matching_box(self, values):
        print("action_add_matching_box === >>>>%s", self)
        for picking in self:
            if picking.picking_type_id.code == "outgoing":
                sale_id = (
                    self.env["sale.order"]
                    .sudo()
                    .search([("name", "=", picking.origin)], limit=1)
                )
                for line in sale_id.order_line:
                    if line.product_id.pack_ids:
                        sale_numbers = line.product_id.pack_ids.mapped("sale_number")
                        product_uom_qty = line.product_uom_qty

                        cases_array, basket, selected_basket = get_selected_boxes(
                            product_uom_qty, sale_numbers
                        )
                        _logger.info("cases_array ===>>> %s", cases_array)
                        _logger.info("basket ===>>> %s", basket)
                        _logger.info("selected_basket ===>>> %s", selected_basket)

                        case_qty = {}
                        for i in range(0, len(cases_array)):
                            case_qty[cases_array[i]] = basket[i]

                        _logger.info("case_qty ===>>> %s", case_qty)

                        sale_packs = self.env["sale.order.packs"].sudo()
                        domain = [("product_id", "=", line.product_id.id)]
                        domain += [("sale_number", "in", selected_basket)]
                        sale_packs_ids = sale_packs.search(
                            domain, order="sale_number desc"
                        )
                        _logger.info("sale_packs_ids === >>>> %s", sale_packs_ids)
                        print(
                            "/len(sale_packs_ids)len(sale_packs_ids)",
                            len(sale_packs_ids),
                        )
                        if len(sale_packs_ids) > 1:
                            for pack_id in sale_packs_ids:
                                if pack_id:
                                    _logger.info("Creating Packages")
                                    out_move = (
                                        self.env["stock.move"]
                                        .sudo()
                                        .create(
                                            {
                                                "name": picking.name,
                                                "product_id": pack_id.product_packaging_id.id,
                                                "location_id": picking.location_id.id,
                                                "location_dest_id": picking.location_dest_id.id,
                                                "product_uom": pack_id.product_packaging_id.uom_id.id,
                                                "product_uom_qty": case_qty.get(
                                                    pack_id.sale_number
                                                )
                                                or 0,
                                                "price_unit": pack_id.product_packaging_id.lst_price,
                                                "picking_id": picking.id,
                                                # "group_id": sale_id.procurement_group_id.id,
                                            }
                                        )
                                    )

                                    _logger.info("out_move ==>>> %s", out_move)
                                    out_move_line = (
                                        self.env["stock.move.line"]
                                        .sudo()
                                        .create(
                                            {
                                                "product_id": pack_id.product_packaging_id.id,
                                                "product_uom_qty": case_qty.get(
                                                    pack_id.sale_number
                                                )
                                                or 0,
                                                "product_uom_id": pack_id.product_packaging_id.uom_id.id,
                                                "location_id": picking.location_id.id,
                                                "company_id": picking.company_id.id,
                                                "date": picking.date,
                                                "location_dest_id": picking.location_dest_id.id,
                                                "picking_id": picking.id,
                                                "move_id": out_move.id,
                                            }
                                        )
                                    )
                                    if pack_id.product_lableing_id:
                                        # out_move = (
                                        #     self.env["stock.move"]
                                        #     .sudo()
                                        #     .create(
                                        #         {
                                        #             "name": picking.name,
                                        #             "product_id": pack_id.product_lableing_id.id,
                                        #             "location_id": picking.location_id.id,
                                        #             "location_dest_id": picking.location_dest_id.id,
                                        #             "product_uom": pack_id.product_lableing_id.uom_id.id,
                                        #             "product_uom_qty": case_qty.get(
                                        #                 pack_id.sale_number
                                        #             )
                                        #             or 0,
                                        #             "price_unit": pack_id.product_lableing_id.lst_price,
                                        #             "picking_id": picking.id,
                                        #             "group_id": sale_id.procurement_group_id.id,
                                        #         }
                                        #     )
                                        # )
                                        #
                                        # _logger.info("out_move ==>>> %s", out_move)
                                        out_move_line = (
                                            self.env["stock.move.line"]
                                            .sudo()
                                            .create(
                                                {
                                                    "product_id": pack_id.product_lableing_id.id,
                                                    "product_uom_qty": case_qty.get(
                                                        pack_id.sale_number
                                                    )
                                                    or 0,
                                                    "product_uom_id": pack_id.product_lableing_id.uom_id.id,
                                                    "location_id": picking.location_id.id,
                                                    "company_id": picking.company_id.id,
                                                    "date": picking.date,
                                                    "location_dest_id": picking.location_dest_id.id,
                                                    "picking_id": picking.id,
                                                    # "move_id": out_move.id,
                                                    # "group_id": sale_id.procurement_group_id.id,
                                                }
                                            )
                                        )
                                    _logger.info(
                                        "out_move_line ==>>> %s", out_move_line
                                    )

                        if len(sale_packs_ids) == 1:
                            for pack_id in sale_packs_ids:
                                if pack_id:
                                    _logger.info("Creating Packages %s", picking.id)
                                    out_move = self.env["stock.move"].create(
                                        {
                                            "name": picking.name,
                                            "product_id": pack_id.product_packaging_id.id,
                                            "location_id": picking.location_id.id,
                                            "location_dest_id": picking.location_dest_id.id,
                                            "product_uom": pack_id.product_packaging_id.uom_id.id,
                                            "product_uom_qty": sum(basket),
                                            "price_unit": pack_id.product_packaging_id.lst_price,
                                            "picking_id": picking.id,
                                            "group_id": sale_id.procurement_group_id.id,
                                        }
                                    )

                                    _logger.info("out_move ==>>> %s", out_move)
                                    out_move_line = (
                                        self.env["stock.move.line"]
                                        .sudo()
                                        .create(
                                            {
                                                "product_id": pack_id.product_packaging_id.id,
                                                "product_uom_qty": sum(basket),
                                                "product_uom_id": pack_id.product_packaging_id.uom_id.id,
                                                "location_id": picking.location_id.id,
                                                "company_id": picking.company_id.id,
                                                "date": picking.date,
                                                "location_dest_id": picking.location_dest_id.id,
                                                "picking_id": picking.id,
                                                "move_id": out_move.id,
                                                # "group_id": sale_id.procurement_group_id.id,
                                            }
                                        )
                                    )
                                    # if pack_id.product_lableing_id:
                                    #     out_move = (
                                    #         self.env["stock.move"]
                                    #         .sudo()
                                    #         .create(
                                    #             {
                                    #                 "name": picking.name,
                                    #                 "product_id": pack_id.product_lableing_id.id,
                                    #                 "location_id": picking.location_id.id,
                                    #                 "location_dest_id": picking.location_dest_id.id,
                                    #                 "product_uom": pack_id.product_lableing_id.uom_id.id,
                                    #                 "product_uom_qty": sum(basket),
                                    #                 "price_unit": pack_id.product_lableing_id.lst_price,
                                    #                 "picking_id": picking.id,
                                    #                 "group_id": sale_id.procurement_group_id.id,
                                    #             }
                                    #         )
                                    #     )
                                    #
                                    #     _logger.info("out_move ==>>> %s", out_move)
                                    #     out_move_line = (
                                    #         self.env["stock.move.line"]
                                    #         .sudo()
                                    #         .create(
                                    #             {
                                    #                 "product_id": pack_id.product_lableing_id.id,
                                    #                 "product_uom_qty": sum(basket),
                                    #                 "product_uom_id": pack_id.product_lableing_id.uom_id.id,
                                    #                 "location_id": picking.location_id.id,
                                    #                 "company_id": picking.company_id.id,
                                    #                 "date": picking.date,
                                    #                 "location_dest_id": picking.location_dest_id.id,
                                    #                 "picking_id": picking.id,
                                    #                 "move_id": out_move.id,
                                    #             }
                                    #         )
                                    #     )

                                    _logger.info(
                                        "out_move_line ==>>> %s", out_move_line
                                    )
            picking.action_confirm()
    @api.model
    def create(self, values):
        result = super(StockPicking, self).create(values)
        result.action_add_matching_box(values)
        return result
