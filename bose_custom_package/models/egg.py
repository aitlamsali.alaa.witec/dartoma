def get_selected_boxes(num_egg, cases_array):
    """[Function returning the number of matching boxes required]

    Args:
        num_egg ([int]): [Number of Eggs]
        cases_array ([array]): [An Array of Cases]

    Returns:
        [array]: [An array of Selected Cases]
    """
    selected_basket = []
    cases_array.sort(reverse=True)
    val = int(num_egg)
    basket = []

    for j in cases_array:
        n_cases = int(val) // j
        val = int(val) % j
        basket.append(n_cases)
    basket.append(val)

    basket = basket[:-1] # order: large to small
    # basket = basket[::] # Reversing the Order

    for i in range(0,len(cases_array)):
        if basket[i] > 0:
            selected_basket.append(cases_array[i])


    return cases_array, basket, selected_basket


#=========================================================
#================TESTING DATA#############################
##########################################################
# cases = [12, 24, 36, 48]
# sell = [9, 12, 16, 24, 30, 36, 40, 48, 56, 60, 72]

# for i in sell:
#     basket = get_selected_boxes(i, cases)
#     print("Number of Eggs : {}".format(int(i)))
#     print(cases)
#     print(basket)
