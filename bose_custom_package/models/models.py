# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __manifest__.py file at the root folder of this module.                  #
###############################################################################

from odoo import models, fields, api
# from odoo.addons.bose_custom_package.models.test import calculte_selected_boxes
from odoo.addons.bose_custom_package.models.egg import get_selected_boxes

import logging
_logger = logging.getLogger(__name__)


class SaleOrder(models.Model):
    _inherit = ['sale.order']

    matching_boxes = fields.Integer(
        string='Package No',
        compute='_compute_matching_boxes')

    @api.onchange('order_line')
    def _compute_matching_boxes(self):
        for record in self:
            matching_boxes = 0
            for line in record.order_line:
                matching_boxes += line.package_no
            record.matching_boxes = matching_boxes


class ProductProduct(models.Model):

    _inherit = ['product.product']

    pack_ids = fields.One2many(
        string='Packs',
        comodel_name='sale.order.packs',
        inverse_name='product_id',
    )


class SaleOrderPacks(models.Model):
    _name = 'sale.order.packs'
    _description = 'Sale Order Packs'

    _rec_name = 'sale_number'
    _order = 'sale_number ASC'

    product_id = fields.Many2one(
        comodel_name='product.product',
        ondelete='cascade',
        required=True
    )
    sale_number = fields.Integer(required=True)
    product_packaging_id = fields.Many2one(
        string='Matching Box',
        comodel_name='product.product',
        ondelete='restrict',
        required=True,
    )
    product_lableing_id = fields.Many2one(
        string='Product Lableing Box',
        comodel_name='product.product',
        ondelete='restrict',
    )
    line_id = fields.Many2one(
        string='Line',
        comodel_name='sale.order.line',
        ondelete='restrict',
    )

    @api.model
    def create(self, values):
        result = super(SaleOrderPacks, self).create(values)
        if result and self.product_id and self.sale_number and self.product_packaging_id:
            if self.product_packaging_id.id not in self.product_id.packaging_ids.ids:
                self.product_id.packaging_ids.ids += self.product_packaging_id.ids
        return result


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    package_no = fields.Integer(
        string='Boxes Required',
        compute='_compute_package_no')
    package_units = fields.Integer(
        string='Package Unit',
        compute='_compute_package_no')
    loose_items = fields.Integer(
        string='Loose Items',
    )
    pack_ids = fields.One2many(
        string='Packs',
        comodel_name='sale.order.packs',
        inverse_name='line_id',
    )

    @api.onchange('product_id', 'product_uom_qty')
    def _compute_package_no(self):
        for record in self:
            record.package_no = package_no = 0
            record.package_units = 0
            try:
                if record.product_id.pack_ids:
                    product_uom_qty = record.product_uom_qty
                    # .filtered(lambda r: r.sale_number >= product_uom_qty)
                    pack_ids = record.product_id.pack_ids
                    sale_numbers = pack_ids.mapped('sale_number')

                    cases_array, basket, selected_basket = get_selected_boxes(
                        product_uom_qty, sale_numbers)
                    
                    _logger.info("cases_array ===>>> %s", cases_array)
                    _logger.info("basket ===>>> %s", basket)

                    record.package_no = sum(basket)
                    sale_packs = self.env['sale.order.packs'].sudo()
                    domain = [('product_id', '=', record.product_id.id)]
                    domain += [('sale_number', 'in', selected_basket)]
                    record.pack_ids = sale_packs.search(domain).ids

            except Exception as e:
                _logger.info("basket ===>>> %s", e.args)
