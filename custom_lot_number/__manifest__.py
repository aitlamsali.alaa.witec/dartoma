# -*- coding: utf-8 -*-
{
    'name': "Custom LOT number",

    'summary': """Custom LOT number""",

    'description': """
       Custom LOT number
    """,

    'author': 'ErpMstar Solutions',
    'category': 'Stock',
    'version': '1.0',
    # any module necessary for this one to work correctly
    'depends': ['stock', 'mrp'],

    # always loaded
    'data': [
        'views/views.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
    'installable': True,
    'application': True,
}
