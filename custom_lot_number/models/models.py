# -*- coding: utf-8 -*-
import datetime

from odoo import models, fields, api, _
from odoo.exceptions import Warning, UserError


class ProductTemplate(models.Model):
    _inherit = "product.template"

    is_main_mfg_product = fields.Boolean("N° Lot Principal en Production")
    is_main_serial_product = fields.Boolean("N° Lot Principal en Achat")



class StockMoveLine(models.Model):
    _inherit = "stock.move.line"
    _order = "id desc"

    @api.model
    def default_get(self, default_fields):
        """If we're creating a new account through a many2one, there are chances that we typed the account code
        instead of its name. In that case, switch both fields values.
        """
        rec = super().default_get(default_fields)
        if  self._context.get('active_model') == 'stock.move' and self._context.get('active_id'):
            move_id = self.env[self._context.get('active_model')].browse(self._context.get('active_id'))
            if move_id.product_id.tracking == "lot" and move_id.picking_id and move_id.picking_id.picking_type_id.code == "incoming" and not move_id.move_line_nosuggest_ids:
                start_date = datetime.date(datetime.date.today().year, 1, 1)
                end_date = datetime.date(datetime.date.today().year, 12, 31)
                rec_count = (
                    self.env["stock.pickamount_toting"].search_count(
                        [
                            ("partner_id", "=", move_id.picking_id.partner_id.id),
                            ("id", "!=", move_id.picking_id.id),
                            ("state", "=", "done"),
                            ("date", ">=", start_date),
                            ("date", "<=", end_date),
                            ("picking_type_id.code", "=", "incoming"),
                        ]
                    )
                )
                default_code = move_id.product_id.default_code
                year = datetime.datetime.today().year
                count = str(rec_count + 1).zfill(3)
                ref = move_id.picking_id.partner_id.ref
                lot_name = (
                    str(default_code)
                    + "/"
                    + str(year)
                    + "/"
                    #+ str(count)
                    + str(self.name).replace("/", "").replace("WH","")
                    + "/"
                    + str(ref)
                )
                rec["lot_name"] = lot_name
                
        return rec


class StockPicking(models.Model):
    _inherit = "stock.picking"

    def button_validate(self):
        for picking in self:
            if picking.picking_type_id.code in ("incoming","mrp_operation"):
                picking.auto_genrate_code()
        return super(StockPicking, self).button_validate()

    def auto_genrate_code(self):
        self.ensure_one()
        if self.picking_type_id.code == "incoming" :
            lot_move_ids = self.move_lines.filtered(
                lambda line: line.product_id.is_main_serial_product
                             and line.product_id.tracking == "lot"
            )
            #3/0
            if lot_move_ids :
                move_ids = self.move_lines.filtered(
                    lambda line: line.product_id.is_main_serial_product
                    and line.product_id.tracking == "lot"
                )
                print("/move_idsmove_idsmove_idsmove_idsmove_ids", move_ids)
                if move_ids:
                    #3 / 0
                    start_date = datetime.date(datetime.date.today().year, 1, 1)
                    end_date = datetime.date(datetime.date.today().year, 12, 31)
                    rec_count = (
                        self.search_count(
                            [
                                ("partner_id", "=", self.partner_id.id),
                                ("id", "!=", self.id),
                                ("state", "=", "done"),
                                ("date", ">=", start_date),
                                ("date", "<=", end_date),
                                ("picking_type_id.code", "=", "incoming"),
                            ]
                        )
                    )
                    for move_id in move_ids:
                        print("./ffffffffffffffffffffffffff", move_id)
                        if not move_id.move_line_nosuggest_ids:
                            default_code = move_id.product_id.default_code
                            year = datetime.datetime.today().year
                            count = str(rec_count + 1).zfill(3)
                            ref = self.partner_id.ref
                            lot_name = (
                                str(default_code)
                                + "/"
                                + str(year)
                                + "/"
                                + str(self.name).replace("/", "").replace("WH","")
                                + "/"
                                + str(ref)
                            )
                            move_id.move_line_nosuggest_ids = [
                                (
                                    0,
                                    0,
                                    {
                                        "lot_name": lot_name,
                                        "qty_done": move_id.product_uom_qty,
                                        "product_uom_id": move_id.product_uom.id,
                                        "location_id": move_id.location_id.id,
                                        "location_dest_id": move_id.location_dest_id.id,
                                        "company_id": move_id.company_id,
                                        "product_id": move_id.product_id.id,
                                        "picking_id": self.id,
                                        "move_id": move_id.id,
                                    },
                                )
                            ]
                else:
                    raise Warning(_("Vous n'avez pas dans cet Achat un produit avec lot principal pour générer le lot"))

        if self.picking_type_id.code == "mrp_operation" :
            lot_move_ids = self.move_lines.filtered(
                lambda line: line.product_id.is_main_mfg_product
                             and line.product_id.tracking == "lot"
            )
            #3/0
            if lot_move_ids :
                move_ids = self.move_lines.filtered(
                    lambda line: line.product_id.is_main_mfg_product
                    and line.product_id.tracking == "lot"
                )
                print("/move_idsmove_idsmove_idsmove_idsmove_ids", move_ids)
                if move_ids:
                    #3 / 0
                    start_date = datetime.date(datetime.date.today().year, 1, 1)
                    end_date = datetime.date(datetime.date.today().year, 12, 31)
                    rec_count = (
                        self.search_count(
                            [
                                ("partner_id", "=", self.partner_id.id),
                                ("id", "!=", self.id),
                                ("state", "=", "done"),
                                ("date", ">=", start_date),
                                ("date", "<=", end_date),
                                ("picking_type_id.code", "=", "incoming"),
                            ]
                        )
                    )
                    for move_id in move_ids:
                        print("./ffffffffffffffffffffffffff", move_id)
                        if not move_id.move_line_nosuggest_ids:
                            default_code = move_id.product_id.default_code
                            year = datetime.datetime.today().year
                            count = str(rec_count + 1).zfill(3)
                            ref = self.partner_id.ref
                            lot_name = (
                                str(default_code)
                                + "/"
                                + str(year)
                                + "/"
                                + str(self.name).replace("/", "").replace("WH","")
                                + "/"
                                + str(ref)
                            )
                            move_id.move_line_nosuggest_ids = [
                                (
                                    0,
                                    0,
                                    {
                                        "lot_name": lot_name,
                                        "qty_done": move_id.product_uom_qty,
                                        "product_uom_id": move_id.product_uom.id,
                                        "location_id": move_id.location_id.id,
                                        "location_dest_id": move_id.location_dest_id.id,
                                        "company_id": move_id.company_id,
                                        "product_id": move_id.product_id.id,
                                        "picking_id": self.id,
                                        "move_id": move_id.id,
                                    },
                                )
                            ]
                else:
                    raise Warning(_("Vous n'avez pas dans cet Production un produit avec lot principal pour générer le lot"))


class MrpProduction(models.Model):
    _inherit = "mrp.production"

    #lot_production_id = fields.Many2one(required=True, )

    def action_generate_serial(self):
        self.ensure_one()

        print("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH", self.move_raw_ids)
        default_lot = True
        main_mfg_product = self.move_raw_ids.filtered(lambda raw_line: raw_line.product_id.product_tmpl_id.is_main_mfg_product and raw_line.product_id.product_tmpl_id.tracking == 'lot')
        if not main_mfg_product:
            raise Warning(_("There is a No any main manufacturing Lot Number Product (is_main_serial_product : x)"))

        if len(main_mfg_product) > 1:
            raise Warning(_("There are multiple main manufacturing Lot  Number Product (is_main_serial_product : x) %s") % (main_mfg_product))

        # for msp in main_serial_product:
        #     if len(msp.move_line_ids) > 1:
        #         raise Warning(_("There are Multiple line in a on serial Product,"))
        move_raw_ids = self.move_raw_ids.filtered(
            lambda raw_line: raw_line.product_id.product_tmpl_id.is_main_mfg_product
            #and raw_line.product_id.product_tmpl_id.is_main_serial_product
            and raw_line.product_id.product_tmpl_id.tracking == "lot"
        )
        print("///////////////////////////////////////", move_raw_ids)

        lot_move_raw_ids = self.move_raw_ids.filtered(
            lambda raw_line: raw_line.product_id.product_tmpl_id.tracking == "lot"
            #and raw_line.product_id.product_tmpl_id.is_main_mfg_product
        )
        #3/0
        print("//////////////////////////", move_raw_ids, lot_move_raw_ids)
        for rec in lot_move_raw_ids:
            print("LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL", rec, rec.move_line_ids, rec.move_line_ids)
            if not rec.move_line_ids :
                #1/0
                raise Warning(_("Choose source lot number for all lot products"))
            else :
                #2/0
                for line in rec.move_line_ids :
                    if not line.lot_id :
                        raise Warning(_("Put lot number for all lot products"))
        #print("sssssssssssssssssssssssss", move_raw_ids)

        if move_raw_ids and move_raw_ids[0].move_line_ids:
            #3/0
            lot_id = move_raw_ids[0].move_line_ids[0].lot_id
            if lot_id:
                lot_name = lot_id.name
                lot_split = lot_name.split("/", 1)
                print("ZZZZZZZZZZZZZZZZZZZZZZZZZZ name split", lot_name, lot_split, type(lot_split))

                if len(lot_split) == 2:
                    #4/0
                    if not self.product_id.default_code :
                        raise Warning(_("Product should have a code : %s", self.product_id.name))
                    lot_suffix = lot_split[1]
                    num_str = self.name.split("/")[-1]
                    num = int(num_str)
                    year = datetime.datetime.today().year
                    #print("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", self.product_id.id)
                    new_lot = (
                        str(self.product_id.default_code)
                        #+ "/"
                        #+ lot_suffix
                        #+ "-"
                        #+ str(num).zfill(3)
                        + "/"
                        + str(year)
                        + "/"
                        + str(self.name).replace("/", "").replace("WH", "")
                        + "/"
                        + str(lot_suffix[-3:])
                        #+ str(ref)

                    )
                    self.lot_producing_id = self.env["stock.production.lot"].create(
                        {
                            "name": new_lot,
                            "product_id": self.product_id.id,
                            "company_id": self.company_id.id,
                        }
                    )
                    default_lot = False
                else :
                    if not self.product_id.default_code :
                        raise Warning(_("Product should have a code : %s", self.product_id.name))
                    lot_suffix = lot_split[0]
                    #num_str = self.name.split("/")[0]
                    #num = int(num_str)
                    year = datetime.datetime.today().year
                    #print("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", self.product_id.id)
                    new_lot = (
                        str(self.product_id.default_code)
                        #+ "/"
                        #+ lot_suffix
                        #+ "-"
                        #+ str(num).zfill(3)
                        + "/"
                        + str(year)
                        + "/"
                        + str(self.name).replace("/", "").replace("WH", "")
                        + "/"
                        + str(lot_suffix[1])
                        #+ str(ref)

                    )
                    self.lot_producing_id = self.env["stock.production.lot"].create(
                        {
                            "name": new_lot,
                            "product_id": self.product_id.id,
                            "company_id": self.company_id.id,
                        }
                    )
                    default_lot = False
            else:
                raise Warning(_("Choose source lot number"))

        # if default_lot:
        #     self.lot_producing_id = self.env["stock.production.lot"].create(
        #         {"product_id": self.product_id.id, "company_id": self.company_id.id}
        #     )
        if self.move_finished_ids.filtered(
            lambda m: m.product_id == self.product_id
        ).move_line_ids:
            self.move_finished_ids.filtered(
                lambda m: m.product_id == self.product_id
            ).move_line_ids.lot_id = self.lot_producing_id
        if self.product_id.tracking == "serial":
            self._set_qty_producing()

    bom_id = fields.Many2one(
        domain="""[
        '&',
            '|',
                ('company_id', '=', False),
                ('company_id', '=', company_id),
            '&',
                '|',
                    ('product_id','=',product_id),
                    '&',
                        ('product_tmpl_id.product_variant_ids','=',product_id),
                        ('product_id','=',False),
        ('type', 'in', ('normal','phantom'))]""")