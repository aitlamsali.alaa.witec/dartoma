odoo.define('report_docxtpl.FormController', function (require) {
"use strict";
    var FormController = require("web.FormController");

    function makeReportUrls(action) {
        let reportUrls = "report/docxtpl/" + action.report_name;
    
        if (
            action.data === undefined ||
            action.data === null ||
            (typeof action.data === "object" &&
                Object.keys(action.data).length === 0)
        ) {
            if (action.context.active_ids) {
                let activeIDsPath = "/" + action.context.active_ids.join(",");
                reportUrls += activeIDsPath;
            }
        } else {
            let serializedOptionsPath =
                "?options=" + encodeURIComponent(JSON.stringify(action.data));
            serializedOptionsPath +=
                "&context=" + encodeURIComponent(JSON.stringify(action.context));
            reportUrls += serializedOptionsPath;
        }
        return reportUrls;
    }

    FormController.include({

        init: function () {
            this._super(...arguments);
            this.direct_print = this.direct_print.bind(this);
            console.log("!!!!!!!!!!!!!!!!!!!!!!!s")
            
        }, 

        on_attach_callback: function () {
            this._super(...arguments);
            window.addEventListener("keydown", this.direct_print);
        },

        on_detach_callback: function () {
            this._super(...arguments);
            window.removeEventListener("keydown", this.direct_print);
        },

        direct_print: async function (ev) {  
            console.log("direct_print")
            console.log(this)    
            if((ev.ctrlKey || ev.metaKey) && (ev.key == "p" || ev.charCode == 16 || ev.charCode == 112 || ev.keyCode == 80)) {
                let directPrintReport = this.toolbarActions.print.find(r => r.direct_print)
                if (directPrintReport) {
                    ev.preventDefault();
                    let docIDS = [this.renderer.state.data.id]
                    this._rpc({
                        model: "ir.actions.report",
                        method: "report_action",
                        args: [directPrintReport.id, docIDS],
                        kwargs: {
                            config: false
                        }
                    }).then(function(action){
                        printJS({printable: makeReportUrls(action), type: 'pdf', showModal: false})
                    })
                }
            }
        }
    })
})
