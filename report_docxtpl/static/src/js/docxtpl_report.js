odoo.define('report_docxtpl.report', function (require) {

    var ActionManager = require('web.ActionManager');
    
    ActionManager.include({
        _executeReportAction: function (action, options) {
            // docxtpl reports
            if ('report_type' in action && action.report_type === 'docxtpl' ) {
                return this._triggerDownload(action, options, 'docxtpl');
            } else {
                return this._super.apply(this, arguments);
            }
        },
    
        _makeReportUrls: function(action) {
            var reportUrls = this._super.apply(this, arguments);
            reportUrls.docxtpl = '/report/docxtpl/' + action.report_name;
            // We may have to build a query string with `action.data`. It's the place
            // were report's using a wizard to customize the output traditionally put
            // their options.
            if (_.isUndefined(action.data) || _.isNull(action.data) ||
                (_.isObject(action.data) && _.isEmpty(action.data))) {
                if (action.context.active_ids) {
                    var activeIDsPath = '/' + action.context.active_ids.join(',');
                    reportUrls.docxtpl += activeIDsPath;;
                }
            } else {
                var serializedOptionsPath = '?options=' + encodeURIComponent(JSON.stringify(action.data));
                serializedOptionsPath += '&context=' + encodeURIComponent(JSON.stringify(action.context));
                reportUrls.docxtpl += serializedOptionsPath;
            }
            return reportUrls;
        }
    });
});