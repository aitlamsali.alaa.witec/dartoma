# -*- coding:utf-8 -*-
{
    "name": "Report Docx",
    "summary": "Export data all objects odoo to Microsoft Office. output files docx, pdf",
    "version": "14.0.1.1.0",
    "category": "Reporting",
    "license": "OPL-1",
    "author": "Lazydoo",
    "price": 29.00,
    "currency": "USD",
    "depends": [
        "base",
        "web",
    ],
    "data": [
        "data/docxtpl_data.xml",
        "security/ir.model.access.csv",
        "views/assets.xml",
        "views/doc_template_view.xml",
        "views/doc_template_menu.xml",
    ],
    "external_dependencies": {
        "python": [
            "docxcompose",
            "docxtpl",
        ],
        "deb": ["libreoffice"],
    },

    "images": [
        "static/description/banner.png",
        "static/description/theme_screenshot.png",
    ],
    "installable": True,
}
